<?php

namespace App\Utility;

/**
 * Cookie:
 *
 * @author Andrew Dyer <andrewdyer@outlook.com>
 * @since 1.0.1
 */
class Session {

    /**
     * Delete:
     * @access public
     * @param string $key
     * @return void
     */
    public static function delete($key): void
    {
        if (self::exists($key)) {
            unset($_SESSION[$key]);
        }
    }

    private static function exists(string $key): bool
    {
        return isset($_SESSION[$key]);
    }

}