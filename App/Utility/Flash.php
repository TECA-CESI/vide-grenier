<?php

namespace App\Utility;

class Flash {

    /**
     * Session: Sets a session message or returns the value of a specific key of
     * the session.
     * @access public
     * @param string $key
     * @param string $value [optional]
     * @return string|null
     * @since 1.0.1
     */
    public static function session($key, $value = "") {
        if (isset($_SESSION[$key])) {
            $session = $_SESSION[$key];
            unset($_SESSION[$key]);
            return $session;
        } elseif (!empty($value)) {
            return($_SESSION[$key] = $value);
        }
        return null;
    }

    /**
     * Danger: Sets a message or returns the value of the SESSION_FLASH_DANGER key of
     * the session.
     * @access public
     * @param string $value [optional]
     * @return string
     * @since 1.0.1
     */
    public static function danger($value = "") {
        return(self::session("danger", $value));
    }

    /**
     * Info: Sets a message or returns the value of the SESSION_FLASH_INFO key of the
     * session.
     * @access public
     * @param string $value [optional]
     * @return string
     * @since 1.0.1
     */
    public static function info($value = "") {
        return(self::session("info", $value));
    }

    /**
     * Success: Sets a message or returns the value of the SESSION_FLASH_SUCCESS key of
     * the session.
     * @access public
     * @param string $value [optional]
     * @return string
     * @since 1.0.1
     */
    public static function success($value = "") {
        return(self::session("success", $value));
    }

    /**
     * Warning: Sets a message or returns the value of the SESSION_FLASH_WARNING key of
     * the session.
     * @access public
     * @param string $value [optional]
     * @return string
     * @since 1.0.1
     */
    public static function warning($value = "") {
        return(self::session("warning", $value));
    }

}