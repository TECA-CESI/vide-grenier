<?php

require 'vendor/autoload.php';
require 'App/Controllers/Api.php';

use App\Controllers\Api;
use PHPUnit\Framework\TestCase;

class ApiTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testProductsAction()
    {
        $params = [];
        $api = new Api($params);

        ob_start();
        $api->ProductsAction();
        $output = ob_get_clean();

        // Vérifier les résultats
        $articles = json_decode($output, true);
        $this->assertIsArray($articles);
        // Ajouter d'autres assertions selon le comportement attendu

        // display test warnings
    }

    /**
     * @throws Exception
     */
    public function testCitiesAction()
    {
        $params = [];
        $api = new Api($params);

        ob_start();
        $api->CitiesAction();
        $output = ob_get_clean();

        $cities = json_decode($output, true);
        $this->assertIsArray($cities);

        var_dump($cities);
    }
}

