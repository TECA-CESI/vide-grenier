<?php

require 'vendor/autoload.php';
require 'App/Controllers/Product.php';
require 'App/Models/Articles.php';

use App\Controllers\Product;
use App\Models\Articles;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testGetAll()
    {
        $pdoMock = $this->createMock(PDO::class);

        $stmtMock = $this->createMock(PDOStatement::class);

        $filter = 'views';
        $stmtMock->expects($this->once())
            ->method('execute')
            ->with([$filter]);

        $stmtMock->expects($this->once())
            ->method('fetchAll')
            ->with(\PDO::FETCH_ASSOC);

        $pdoMock->expects($this->once())
            ->method('prepare')
            ->with('SELECT * FROM articles ORDER BY articles.views DESC')
            ->willReturn($stmtMock);

        Articles::getAll($filter);

        $this->addToAssertionCount(1);
    }

    /**
     * @throws Exception
     */
    public function testGetOne()
    {
        $pdoMock = $this->createMock(PDO::class);

        $stmtMock = $this->createMock(PDOStatement::class);

        $id = 1;
        $stmtMock->expects($this->once())
            ->method('execute')
            ->with([$id]);

        $stmtMock->expects($this->once())
            ->method('fetchAll')
            ->with(\PDO::FETCH_ASSOC);

        $pdoMock->expects($this->once())
            ->method('prepare')
            ->with('
                SELECT * FROM articles
                INNER JOIN users ON articles.user_id = users.id
                WHERE articles.id = ? 
                LIMIT 1')
            ->willReturn($stmtMock);

        Articles::getOne($id);

        $this->addToAssertionCount(1);
    }

    /**
     * @throws Exception
     */
    public function testAddOneView()
    {
        $dbMock = $this->getMockBuilder(\PDO::class)
            ->disableOriginalConstructor()
            ->getMock();

        $stmtMock = $this->getMockBuilder(PDOStatement::class)
            ->disableOriginalConstructor()
            ->getMock();

        $stmtMock->expects($this->once())
            ->method('execute')
            ->with([1]);

        $dbMock->expects($this->once())
            ->method('prepare')
            ->with('UPDATE articles SET articles.views = articles.views + 1 WHERE articles.id = ?')
            ->willReturn($stmtMock);

        Articles::addOneView(1);

        $this->addToAssertionCount(1);
    }

    /**
     * @throws Exception
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testGetByUser()
    {
        $pdoMock = $this->createMock(PDO::class);

        $stmtMock = $this->createMock(PDOStatement::class);

        $id = 1;
        $stmtMock->expects($this->once())
            ->method('execute')
            ->with([$id]);

        $pdoMock->expects($this->once())
            ->method('prepare')
            ->with('SELECT *, articles.id as id FROM articles LEFT JOIN users ON articles.user_id = users.id WHERE articles.user_id = ?')
            ->willReturn($stmtMock);

        var_dump($pdoMock);

        $articleClass = $this->getMockBuilder(Product::class)
            ->disableOriginalConstructor()
            ->getMock();

        $articleClass->expects($this->once())
            ->method('getDB')
            ->willReturn($pdoMock);

        $expected = [
            [
                'id' => 1,
                'title' => 'test',
                'content' => 'test',
                'user_id' => 1,
                'views' => 0,
                'created_at' => '2020-01-01 00:00:00',
                'updated_at' => '2020-01-01 00:00:00',
                'email' => ''
            ]
        ];

        $result = Articles::getByUser($id);

        $this->assertEquals($result, $expected);
    }

}