<?php

require 'vendor/autoload.php';
require 'App/Controllers/Home.php';

use App\Controllers\Home;
use PHPUnit\Framework\TestCase;

class HomeTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testIndexAction()
    {
        $params = [];
        $home = new Home($params);

        ob_start();
        $home->indexAction();
        $output = ob_get_clean();

        // Vérifier les résultats
        $this->assertStringContainsString('Vide Grenier en ligne', $output);
        // Ajouter d'autres assertions selon le comportement attendu

        // display test warnings
        var_dump($output);
    }
}