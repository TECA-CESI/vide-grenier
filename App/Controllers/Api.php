<?php

namespace App\Controllers;

use App\Models\Articles;
use App\Models\Cities;
use \Core\View;
use Exception;
use OpenApi\Annotations as OA;

/**
 * API controller
 * @OA\Info(title="Vide Grenier en ligne", version="0.1")
 * @OA\Server(url="http://localhost:80")
 */
class Api extends \Core\Controller
{

    /**
     * Affiche la liste des articles / produits pour la page d'accueil
     * Avec comme paramètre optionnel "sort" pour trier les articles par date ou par prix
     *
     * @throws Exception
     * @OA\Get(
     *     path="/products",
     *     summary="Affiche la liste des articles / produits pour la page d'accueil",
     *     description="Affiche la liste des articles / produits pour la page d'accueil",
     *     @OA\Response(
     *     response=200,
     *     description="Affiche la liste des articles / produits pour la page d'accueil"
     *     ))
     *     @OA\Parameter(
     *     name="sort",
     *     in="query",
     *     description="Trier les articles par date ou par prix",
     *     required=false,
     *     @OA\Schema(
     *     type="string",
     *     enum={"date", "price"}
     *     )
     *    )
     * )
     */

    public function ProductsAction()
    {
        $query = $_GET['sort'];

        $articles = Articles::getAll($query);

        header('Content-Type: application/json');
        echo json_encode($articles);
    }

    /**
     * Recherche dans la liste des villes
     *
     *
     * @throws Exception
     * @OA\Get(
     *     path="/cities",
     *     summary="Recherche dans la liste des villes",
     *     description="Recherche dans la liste des villes",
     *     @OA\Response(
     *      response=200,
     *      description="Recherche dans la liste des villes",
     *     )
     * )
     */
    public function CitiesAction()
    {

        $cities = Cities::search($_GET['query']);

        header('Content-Type: application/json');
        echo json_encode($cities);
    }

}
