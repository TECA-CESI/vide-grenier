<?php

namespace App\Controllers;

use \Core\View;
use Exception;

use App\Models\Articles;
use App\Models\User;

/**
 * Dashboard controller
 */
class Dashboard extends \Core\Controller
{

    /**
     * Affiche la page d'accueil
     *
     * @return void
     * @throws \Exception
     */
    public function indexAction()
    {
        $articles = Articles::getAll('views');

        View::renderTemplate(
            'Dashboard/index.html',
            []
        );
    }
}
