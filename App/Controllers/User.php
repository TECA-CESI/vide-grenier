<?php

namespace App\Controllers;

use App\Config;
use App\Model\UserRegister;
use App\Models\Articles;
use App\Utility\Cookie;
use App\Utility\Flash;
use App\Utility\Hash;
use App\Utility\Session;
use Core\Controller;
use \Core\View;
use Exception;
use http\Env\Request;
use http\Exception\InvalidArgumentException;
use PHPMailer\PHPMailer\PHPMailer;


/**
 * User controller
 */
class User extends Controller
{

    /**
     * Affiche la page de login
     */
    public function loginAction()
    {
        if (isset($_POST['submit'])) {
            $f = $_POST;

            if (empty($f['email']) || empty($f['password'])) {
                Flash::danger('Veuillez remplir tous les champs');
            }

            $this->login($f);

            // Si login OK, redirige vers le compte
            header('Location: /account');
        }

        View::renderTemplate('User/login.html');
    }

    /**
     * Page de création de compte
     */
    public function registerAction()
    {
        if (isset($_POST['submit'])) {
            $f = $_POST;

            if ($f['password'] !== $f['password-check']) {
                Flash::danger('Les mots de passe ne correspondent pas');
            }

            if ($f['password'] === '') {
                Flash::danger('Veuillez remplir tous les champs');
            }

            if ($f['email'] === '') {
                Flash::danger('Veuillez remplir tous les champs');
            }

            if ($f['username'] === '') {
                Flash::danger('Veuillez remplir tous les champs');
            }

            if ($f['password-check'] === '') {
                Flash::danger('Veuillez remplir tous les champs');
            }

            // validation

            if ($this->register($f) !== false) {
                $this->login($f);
                header('Location: /account');
            }
        }

        View::renderTemplate('User/register.html');
    }

    /**
     * Affiche la page du compte
     */
    public function accountAction()
    {
        $articles = Articles::getByUser($_SESSION['user']['id']);

        View::renderTemplate('User/account.html', [
            'articles' => $articles
        ]);
    }

    /*
     * Fonction privée pour enregister un utilisateur
     */
    private function register($data)
    {
        try {
            // Generate a salt, which will be applied to the during the password
            // hashing process.
            $salt = Hash::generateSalt(32);

            $userID = \App\Models\User::createUser([
                "email" => $data['email'],
                "username" => $data['username'],
                "password" => Hash::generate($data['password'], $salt),
                "salt" => $salt
            ]);

            return $userID;

        } catch (Exception $ex) {
            Flash::danger($ex->getMessage());
        }
    }

    private function login($data)
    {
        try {
            if (!isset($data['email']) || $data['email'] === '' || !isset($data['password']) || $data['password'] === '') {
                Flash::danger('Veuillez remplir tous les champs');
            }

            $user = \App\Models\User::getByLogin($data['email']);

            if (Hash::generate($data['password'], $user['salt']) !== $user['password']) {
                return false;
            }

            if (isset($data['remember']) && $data['remember'] == 'on') {
                $token = Hash::generateSalt(32);
                $expiry = Config::USER_COOKIE_EXPIRY;
                Cookie::put($user['id'], $token, $expiry);
                setcookie('remember', $token, time() + 3600 * 24 * 30, '/');
            }

            $_SESSION['user'] = array(
                'id' => $user['id'],
                'username' => $user['username'],
            );

            return true;

        } catch (Exception $ex) {
            Flash::danger($ex->getMessage());
        }
    }


    /**
     * Logout: Delete cookie and session. Returns true if everything is okay,
     * otherwise turns false.
     * @access public
     * @return boolean
     * @since 1.0.2
     */
    public function logoutAction()
    {
        $cookies = $_COOKIE;

        foreach ($cookies as $cookie_name => $cookie_value) {
            setcookie($cookie_name, '', time() - 3600, '/');
        }

        $_SESSION = array();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 3600, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        }

        header("Location: /");

        session_destroy();

        return true;
    }

    public function sendMailAction()
    {
        $id = $this->route_params['id'];
        $User =  Articles::getUserFromProduct($id);
        $Product = Articles::getOne($id);

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->Mailer = "smtp";
        $mail->SMTPDebug  = 1;
        $mail->SMTPAuth   = TRUE;
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Host       = "smtp.gmail.com";
        $mail->Username   = "hamoncelian@gmail.com";
        $mail->Password   = "pass";
        $mail->IsHTML(true);
        $mail->AddAddress("recipient-email@domain.fr", "recipient-name");
        $mail->SetFrom("from-email@gmail.com", "from-name");
        $mail->AddReplyTo("reply-to-email@domain.fr", "reply-to-name");
        $mail->AddCC("cc-recipient-email@domain.fr", "cc-recipient-name");
        $mail->Subject = "Test is Test Email sent via Gmail SMTP Server using PHP Mailer";
        $content = "<b>This is a Test Email sent via Gmail SMTP Server using PHP mailer class.</b>";
        $mail->MsgHTML($content);

        header('Location: /product/'.$id);
    }

}
