start:
	docker-compose up --build -d
stop:
	docker-compose down
test:
	docker exec php-videgrenier vendor/bin/phpunit ./App/Tests/ApiTest.php
